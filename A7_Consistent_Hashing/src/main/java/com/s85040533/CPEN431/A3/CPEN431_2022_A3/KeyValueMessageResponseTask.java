package com.s85040533.CPEN431.A3.CPEN431_2022_A3;

import java.util.AbstractMap.SimpleEntry;
import java.net.InetAddress;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.google.protobuf.ByteString;
import com.s85040533.CPEN431.A3.CPEN431_2022_A3.RequestReplyProtocolHandle.ReceivedRequest;

import ca.NetSysLab.ProtocolBuffers.KeyValueRequest.KVRequest;
import ca.NetSysLab.ProtocolBuffers.KeyValueResponse.KVResponse;
import ca.NetSysLab.ProtocolBuffers.KeyValueResponse.KVResponse.Builder;
import ca.NetSysLab.ProtocolBuffers.Message.Msg;

public class KeyValueMessageResponseTask implements Runnable {
  
 
   protected static ConcurrentHashMap<ByteString, Builder> cachedResponses= new ConcurrentHashMap<ByteString, Builder>( );
   protected static ConcurrentLinkedQueue<SimpleEntry<Date,  ByteString>> cachedHeaderIds = new ConcurrentLinkedQueue<SimpleEntry<Date,  ByteString>>();
   protected KeyValueHandle valueHandle ;
   protected RequestReplyProtocolHandle protocolHandle;
   protected MessageHandle msgHandle;
   protected ReceivedRequest request;
   protected boolean needShutDown;
   protected static int cachedValueSize =0;
   private static Integer pid=null;
   
    public KeyValueMessageResponseTask(KeyValueHandle keyValueHandle,RequestReplyProtocolHandle pHandle,  MessageHandle mHandle,ReceivedRequest r)  {
      
        protocolHandle = pHandle;
        msgHandle = mHandle;
        valueHandle = keyValueHandle;
        request = r;
        needShutDown = false;
   
        
    }
    
    public static int getPID() {
    	if ( pid == null) {
    		int result =0;
            String processName = java.lang.management.ManagementFactory.getRuntimeMXBean().getName();
            if (processName != null && processName.length() > 0) {
                
                 result =  Integer.parseInt(processName.split("@")[0]);
                
            }

            pid = result;
    	}
    	
        return pid;
    }	 
    
    public static void cleanLegacyCache() {
    	Date now = new Date();
    
    	boolean hasDeletedRecord = false;
    	Builder kvBuilder = null;

    	SimpleEntry<Date,  ByteString> topHeader = cachedHeaderIds.peek(); 
    	while ( topHeader != null  ) {
    		if ( (int)((now.getTime()- topHeader.getKey().getTime())/1000) >= 5 ) {
    			kvBuilder = cachedResponses.remove(topHeader.getValue());
    	    	if (kvBuilder != null) {
//     				cachedValueSize -= kvBuilder.build().toByteString().size();
    	    		cachedValueSize -= kvBuilder.getValue().size();
        			cachedHeaderIds.poll();
        			hasDeletedRecord = true;
    	    	}
    		}else
    			break;
    		topHeader = cachedHeaderIds.peek(); 
    	}
    	
    	if ( hasDeletedRecord )
    	 System.gc();
    	 
    }
    
    protected static void pushCacheResponse(ByteString messageId, Builder builder) {
    	if (builder != null) {
//        	cachedValueSize += builder.build().toByteString().size();
    		cachedValueSize += builder.getValue().size();
//        	cachedResponses.put(messageId, CompressionUtils.compress(payload.toByteArray()) );
        	cachedResponses.put(messageId, builder);
        	cachedHeaderIds.add(new SimpleEntry<Date,ByteString>(new Date(), messageId));
    	}
    }
    public void run() {
 
        System.out.println("Running KeyValueMessageReponseTask");
              
    	 
        Builder responseBuilder = KVResponse.newBuilder();
        Msg repliedMessage = null;
        boolean isSuccess = false;
		Runtime runtime =  Runtime.getRuntime();
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
		dateFormat.setTimeZone(TimeZone.getTimeZone("PST"));
		String currentTime;
        try{
                if ( msgHandle.isValid( request.message ) )
                {
                	System.out.println("MessageValid");
                	Builder cachedResponse = cachedResponses.get(request.message.getMessageID());
                	if (cachedResponse != null) {
//                    		repliedMessage = msgHandle.getMessage(CompressionUtils.decompress(cachedResponse) ,request.message.getMessageID().toByteArray());
                   		repliedMessage = msgHandle.getMessage(cachedResponse.build().toByteArray() ,request.message.getMessageID().toByteArray());
                	}
                	
                	if ( repliedMessage == null) {
                		if ( cachedValueSize <= App.cacheLimit) {
                			
                			
                			
	                		KVRequest kvRequest = KVRequest.parseFrom(request.message.getPayload().toByteArray());
	                    	if ( kvRequest.hasCommand())
	                    	{
	                			int command =  kvRequest.getCommand();
	                		    if ( command == RequestCommand.GETPID.toInt() ) {
	                    			responseBuilder.setPid(getPID());
	                    			isSuccess = true;
	                    		}else if ( command == RequestCommand.GETMEMBERSHIPCOUNT.toInt()) {
	                    			responseBuilder.setMembershipCount(1);
	                    			isSuccess = true;
	                    		}else if ( command == RequestCommand.ISALIVE.toInt()) {
	                    			
	                    			isSuccess = true;
	                    		}else if ( command == RequestCommand.SHUTDOWN.toInt()) {
	                    			needShutDown = true;
	                    			//Seems not good
	                    			System.exit(0);
	                    		}else
	                    		{
	                    			
	                    			isSuccess = valueHandle.process(kvRequest, responseBuilder);
	                    			
	                    			
	                    		}
	                    	}else {
	                    		responseBuilder.setErrCode( ReplyErrorCode.UNRECOGN.toInt() );
	                    	}
                		}else {
                			
                			responseBuilder.setErrCode(ReplyErrorCode.OVERLOAD.toInt());
                			responseBuilder.setOverloadWaitTime(App.overloadWaitTime);
//                	        System.out.println("Overload due to cache.");
//                	        // creating a new object of the class Date  
//                	        java.util.Date date = new java.util.Date();    
//                			currentTime = dateFormat.format(date);
//            				System.out.println(currentTime + " KV Store Used/Total: " + valueHandle.numberOfByteUsed + "/" + App.minKVStoreSize);
//            				System.out.println(currentTime + " Cache Used/Total: " + cachedValueSize + "/" + App.cacheLimit);
//            				System.out.println(currentTime + " Free/Used Memory: " + (runtime.maxMemory() - runtime.totalMemory() + runtime.freeMemory()) + "/" + (runtime.totalMemory() - runtime.freeMemory()));
//          				System.out.println("KV Store Used/Total: " + valueHandle.numberOfByteUsed + "/" + App.minKVStoreSize + " Cache Used/Total: " + cachedValueSize + "/" + App.cacheLimit + " Free/Used Memory: " + (runtime.maxMemory() - runtime.totalMemory() + runtime.freeMemory()) + "/" + (runtime.totalMemory() - runtime.freeMemory()));
                		}
                	}
                	
                		
                }else{
            	
            	//TODO: Error code for invalid checksum?
	                	responseBuilder.setErrCode( ReplyErrorCode.UNRECOGN.toInt()  );
	              }
            
        	if ( !needShutDown ) {
        		
        		if ( repliedMessage == null ) {
        			if ( isSuccess ) {
                    	responseBuilder.setErrCode(ReplyErrorCode.SUCCESSFUL.toInt());
                    	
                    }else if (!responseBuilder.hasErrCode()) {
                 
                    	responseBuilder.setErrCode(ReplyErrorCode.INTERNALFAILURE.toInt());
                    }
                    
        		 
        	        repliedMessage = msgHandle.getMessage(responseBuilder.build().toByteArray(),request.message.getMessageID().toByteArray());
                 	if ( responseBuilder.getErrCode() != ReplyErrorCode.OVERLOAD.toInt())
                 		pushCacheResponse(request.message.getMessageID(), responseBuilder);
                 
                   
                     
                    	
        		} 
        		
        		if ( repliedMessage != null ) {
        			if (request.message.hasClient()) {
        				byte[] addr = request.message.getClient().getAddress().toByteArray();
                        int port = request.message.getClient().getPort();

                		protocolHandle.send(InetAddress.getByAddress(addr), port, repliedMessage);

        			} else {
        				protocolHandle.send(request.requestIPAddress, request.requestPortNumber, repliedMessage);
        			}
        		}
        		//System.out.println("Sent");
        	}
        }catch (Exception exp) {
        	exp.printStackTrace();
            System.out.println("Exception : " + exp.getMessage());
        }

        	 
       
       
    }
		 
    public void localRun(ReceivedRequest r) {
    	request = r;
    	run();
    }
    
}
