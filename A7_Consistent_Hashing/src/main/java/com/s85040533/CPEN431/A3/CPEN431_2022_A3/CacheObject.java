package com.s85040533.CPEN431.A3.CPEN431_2022_A3;

import com.google.protobuf.ByteString;

public class CacheObject {

	public CacheObject(int ver, ByteString val) {
		value = val;
		version = ver;
	}
	
	public ByteString value;
	public int version;
}
