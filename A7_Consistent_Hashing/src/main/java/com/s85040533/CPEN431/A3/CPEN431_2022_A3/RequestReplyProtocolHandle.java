package com.s85040533.CPEN431.A3.CPEN431_2022_A3;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.net.SocketException;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;

import ca.NetSysLab.ProtocolBuffers.Message;
import ca.NetSysLab.ProtocolBuffers.Message.Msg;

public class RequestReplyProtocolHandle {
	
	private short inPortNumber;
	
	MessageHandle messageHandle;
	private DatagramSocket  socket;
	 private byte[] bufBtyes;
	public class ReceivedRequest{
		
		public ReceivedRequest(Msg msg, InetAddress requestIP, int portNumber) {
			message = msg;
			requestIPAddress=requestIP ;
			requestPortNumber = portNumber ;
			numberOfRetry = 0 ;
		}
		public Msg message;
	    public InetAddress requestIPAddress;
        public int requestPortNumber ;
        public int numberOfRetry ;
	}
	public RequestReplyProtocolHandle(short inPort,  MessageHandle msgHandle) {
		inPortNumber = inPort;
		
		messageHandle = msgHandle;
		bufBtyes = new byte[messageHandle.getMaxMessageSize() ];
		try {
			socket = new DatagramSocket(inPortNumber);
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public ReceivedRequest receive(int timeOutValue ) throws IOException{
		
		   
	   if ( socket == null )
		   socket = new DatagramSocket(inPortNumber);
	   
       Msg receiveMessage = null;
       
       if ( timeOutValue > 0)
    	   socket.setSoTimeout(timeOutValue);
       // receive request
       DatagramPacket receivePacket = new DatagramPacket(bufBtyes, bufBtyes.length);
       socket.receive(receivePacket);
       receiveMessage = Message.Msg.parseFrom(
	    		Arrays.copyOfRange(receivePacket.getData(), 0, receivePacket.getLength())
	    		);
       
       return new ReceivedRequest(receiveMessage, receivePacket.getAddress(),receivePacket.getPort() );
			
	}
	
	public  boolean send(InetAddress address, int portNumber,  Msg requestMsg){
		
		 
	
		
		boolean success = false;
	
		
	  
	 
		try {
			
			if ( socket == null )
				socket = new DatagramSocket(inPortNumber);
			
			byte[] request = requestMsg.toByteArray();
			
			 
			DatagramPacket	packet = new DatagramPacket(request, request.length, address, portNumber);
			
			 
			socket.send(packet);
		

			
			success = true;
		
		} catch (IOException e) {
			 System.out.println("Send message Exception : " + e.getMessage());
		}
		 

			
		 
 
	
	
		return success;
	
		
		
		
	}
	public  ReceivedRequest sendAndReceive(InetAddress address, int portNumber,  Msg requestMsg,int timeOutInit,int resentTimeOutInSecond, int numOfFailureRetry) throws IOException {
		
	 
		 
		ReceivedRequest result = null;
	
		int retryNumber = 1, timeoutValue = timeOutInit;
	 
		boolean isSuccess = false;
		Instant startRequestTime = null;
		IOException exception = new IOException();
 
		Duration retryDuration;
		do {

			try {
				//TODO : There is a room to improve the performance
				if ( startRequestTime == null )
					startRequestTime = Instant.now();
				if ( this.send(address, portNumber, requestMsg) ) {
					result = this.receive(timeoutValue);
					
					if (result.message != null &&  result.message.getMessageID().equals(requestMsg.getMessageID())) {
						isSuccess = true;
						result.numberOfRetry = retryNumber - 1;
					}
					else
						result = null;
					
				}
				System.out.println("Num of Retry : " + (retryNumber - 1));
					

				
			
			} catch (java.net.SocketTimeoutException e) {
				System.out.println("Timeout Exception : " + e.toString() );
				exception = e;
			} catch (IOException e) {
				exception = e;
			}


			
			if ( !isSuccess ) {

				retryNumber++;
				timeoutValue *= 2;
			}
			Instant now = Instant.now();
			retryDuration = Duration.between(startRequestTime, now);

		} while (retryNumber <= numOfFailureRetry && !isSuccess && resentTimeOutInSecond >= retryDuration.getSeconds());

		if ( !isSuccess ) {
			throw exception;
		}
		 
//		System.out.println("retryNumber : " + retryNumber );
	
		return result;
	
		
		
		
	}
}
