package com.s85040533.CPEN431.A3.CPEN431_2022_A3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

import javax.print.DocFlavor.URL;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.s85040533.CPEN431.A3.CPEN431_2022_A3.RequestReplyProtocolHandle.ReceivedRequest;

import ca.NetSysLab.ProtocolBuffers.KeyValueRequest;
import ca.NetSysLab.ProtocolBuffers.KeyValueRequest.KVRequest.Builder;
import ca.NetSysLab.ProtocolBuffers.KeyValueResponse.KVResponse;
import ca.NetSysLab.ProtocolBuffers.Message.Msg;

public class Client {

	private final int NORESPONSE = -1;
	private final int WRONGRESULT = -2;
	private final int EXCEPTION = -3;
	private final int TIMEOUT = -4;
	protected short clientPortNumber = 5553;
	protected String serverIPAddress ;
	protected short serverPortNumber;
	protected RequestReplyProtocolHandle protocolHandle;
	protected MessageHandle messageHandle;
	protected String serverListFilePath = "server.list";
	protected String logFilePath = "a3.log";
	protected String serverIPAddressFromFile;
	protected String serverPortNumberFromFile;
	protected boolean firstWrite = true;

	
	public Client(String serverListFilePath)
	{
		messageHandle = new MessageHandle(clientPortNumber,16384);
		protocolHandle = new RequestReplyProtocolHandle(clientPortNumber,messageHandle  );
		this.serverListFilePath = serverListFilePath;
		
		serverPortNumber = 4445;
		try {
			serverIPAddress = InetAddress.getLocalHost().getHostAddress();
//			System.out.println("Server IP : " + serverIPAddress);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 
	}
	
	public class ReceivedResponseResult{
		
		public ReceivedResponseResult(int resultCode, String returnValue, int numberOfRetry) {
			this.resultCode = resultCode;
			this.returnValue = returnValue ;
			this.numberOfRetry = numberOfRetry ;
		}
		public int resultCode;
	    public String returnValue;
        public int numberOfRetry ;
	}

	public void close() {
		//protocolHandle.close();
	}
	
	public void Test() {
		
	    // creating a new object of the class Date  
	    java.util.Date date = new java.util.Date();    

	    //testShutdown();
		/*
		 * testPut("12345","V12345",3); testGet("12345"); testRemove("12345");
		 * testGet("12345"); testPut("12345","V12345",null); testGet("12345");
		 */
		try {
			final long startTime = System.currentTimeMillis();
			String buildDateTime;

			String jarPath = Client.class
				.getProtectionDomain()
				.getCodeSource()
				.getLocation()
				.toURI()
				.getPath();
            // Get name of the JAR file
            String jarName = jarPath.substring(jarPath.lastIndexOf("/") + 1);
//            System.out.printf("JAR Name: " + jarName);
//            writeLineToLogFile("1. " + jarName + "\n");
			JarFile jf = new JarFile(jarName);
			ZipEntry manifest = jf.getEntry("META-INF/MANIFEST.MF");
			long manifestTime = manifest.getTime();  //in standard millis
			DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.ENGLISH);
			dateFormat.setTimeZone(TimeZone.getTimeZone("PST"));
			buildDateTime = dateFormat.format(manifestTime);
			
			writeLineToLogFile("CPEN 431 BONUS Test Client\n");
			writeLineToLogFile("Time: " + date + "\n");
			writeLineToLogFile("Build Date: " + buildDateTime + "\n");
			writeLineToLogFile("Java process ID: " + ServerThread.getPID() + "\n");
			writeLineToLogFile("[A3 BONUS TESTS]" + "\n\n");
			if (readServerListFile()) {
				serverIPAddress = serverIPAddressFromFile;
				serverPortNumber = Short.valueOf(serverPortNumberFromFile);
				writeLineToLogFile("[Info] Created server list successfully.\n\n");

				//Basic test cases to test each command.
//				testPutEmptyKey("", 10000); // PUT with empty key.
//				testGetEmptyKey(""); // GET with empty key.
//				testOnlyPut("12345", 10000); // PUT when there is nothing.
//				testOnlyGet("23456"); // GET when there is nothing.
//				testOnlyRemove("23456"); // REMOVE when there is nothing.
////				testOnlyShutdown(); // Just shut down the server.
//				testOnlyWipeout(); // Just test Wipeout.
//				testOnlyServerAlive(); // Just test server alive.
//				testOnlyGetProcessID(); // Just test Get Process ID.
//				testOnlyInvaildCommand(); // Just test invalid command.
//				testOnlyGetMembershipCount(); // Just test Get Membership Count.

				//Boundary test cases.
				testPutGet("01234", 1500, ReplyErrorCode.SUCCESSFUL.toInt(), ReplyErrorCode.SUCCESSFUL.toInt()); // Discover cache limit which affect all following commands
//				testPutGet("ABCD0", 1550, ReplyErrorCode.SUCCESSFUL.toInt(), ReplyErrorCode.SUCCESSFUL.toInt());
//				testPutGet("ABCD1", 2000, ReplyErrorCode.SUCCESSFUL.toInt(), ReplyErrorCode.SUCCESSFUL.toInt());
//				testPutGet("ABCD2", 3000, ReplyErrorCode.SUCCESSFUL.toInt(), ReplyErrorCode.SUCCESSFUL.toInt());
//				testPutGet("ABCD3", 4000, ReplyErrorCode.SUCCESSFUL.toInt(), ReplyErrorCode.SUCCESSFUL.toInt());
//				testPutGet("ABCD4", 5000, ReplyErrorCode.SUCCESSFUL.toInt(), ReplyErrorCode.SUCCESSFUL.toInt());
//				testPutGet("ABCD5", 6000, ReplyErrorCode.SUCCESSFUL.toInt(), ReplyErrorCode.SUCCESSFUL.toInt());
//				testPutGet("ABCD6", 7000, ReplyErrorCode.SUCCESSFUL.toInt(), ReplyErrorCode.SUCCESSFUL.toInt());
//				testPutGet("ABCD7", 8000, ReplyErrorCode.SUCCESSFUL.toInt(), ReplyErrorCode.SUCCESSFUL.toInt());
//				testPutGet("ABCD8", 9000, ReplyErrorCode.SUCCESSFUL.toInt(), ReplyErrorCode.SUCCESSFUL.toInt());
//				testPutGet("12345", 10000, ReplyErrorCode.SUCCESSFUL.toInt(), ReplyErrorCode.SUCCESSFUL.toInt()); // TEST Basic PUT/GET (PUT -> GET). Value Length = 10000 Bytes
//				testPutGet("12345678901234567890123456789012", 10000, ReplyErrorCode.SUCCESSFUL.toInt(), ReplyErrorCode.SUCCESSFUL.toInt()); // TEST Basic PUT/GET (PUT -> GET). Key Length = 32 Bytes and Value Length = 10000 Bytes
//				testPutGet("23456", 10001, ReplyErrorCode.INVALIDVAL.toInt(), ReplyErrorCode.NONEXISTKEY.toInt()); // TEST Basic PUT/GET (PUT -> GET). Value Length = 10001 Bytes
//				testPutGet("123456789012345678901234567890123", 10000, ReplyErrorCode.INVALIDKEY.toInt(), ReplyErrorCode.INVALIDKEY.toInt()); // TEST Basic PUT/GET (PUT -> GET). Key Length = 33 Bytes and Value Length = 10000 Bytes
//				testPutGet("01234567890123456789012345678901", 10001, ReplyErrorCode.INVALIDVAL.toInt(), ReplyErrorCode.NONEXISTKEY.toInt()); // TEST Basic PUT/GET (PUT -> GET). Key Length = 32 Bytes and Value Length = 10001 Bytes
//				testPutGet("012345678901234567890123456789012", 10001, ReplyErrorCode.INVALIDKEY.toInt(), ReplyErrorCode.INVALIDKEY.toInt()); // TEST Basic PUT/GET (PUT -> GET). Key Length = 33 Bytes and Value Length = 10001 Bytes
//				testPutGetRemoveGet("12345", 10000); // TEST Basic REMOVE (PUT -> GET -> REM -> GET). No exception key / value.

//				writeLineToLogFile("Free Memory: " + Runtime.getRuntime().freeMemory() + "\n");
//				testPutWipeoutGet("23456", 10000); // TEST Basic PUT/Wipeout/GET (PUT -> Wipeout -> GET).
//				writeLineToLogFile("Free Memory: " + Runtime.getRuntime().freeMemory() + "\n");
//				testPutWipeoutRemove("34567", 10000); // TEST Basic PUT/Wipeout/REMOVE (PUT -> Wipeout -> REMOVE).
				writeLineToLogFile("Free Memory: " + Runtime.getRuntime().freeMemory() + "\n");
//				testPutGetClosedLoop(1000);
//				writeLineToLogFile("Free Memory: " + Runtime.getRuntime().freeMemory() + "\n");

//				testOnlyShutdown(); // Just shut down the server.
//				testOnlyWipeout(); // This Wipeout tests if the shutdown works.
			}
			else {
				writeLineToLogFile("Unable to parse the server list file!\n");
				writeLineToLogFile("Error building server list file!\n");
			}

			final long endTime = System.currentTimeMillis();
			NumberFormat formatter = new DecimalFormat("#0.00000");
//			System.out.print("Execution time is " + formatter.format((endTime - startTime) / 1000d) + " seconds");
			
			writeLineToLogFile("Completed A3 BONUS Tests.\n");
			writeLineToLogFile("Testing completed in " + formatter.format((endTime - startTime) / 1000d) + "s\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			writeLineToLogFile("Test Exception : " + e.toString() + "\n");
	        try {
				e.printStackTrace(new PrintStream(new FileOutputStream(logFilePath, true), true));
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			writeLineToLogFile("Test Exception : " + e.toString() + "\n");
	        try {
				e.printStackTrace(new PrintStream(new FileOutputStream(logFilePath, true), true));
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
//		testPut("12345","V12345",3);
//		testGet("12345");
//		testIsAlive();
	}

	public void testPutEmptyKey(String key, int numberOfBytes) {
		String dummyValue = "";
		
		writeLineToLogFile("[ TEST PUT with empty key.]\n");
		for (int i = 0;i < numberOfBytes;i++) {
			dummyValue = dummyValue + "0"; 
		}
		if (getPutResultCode(key, dummyValue, 0) == ReplyErrorCode.SUCCESSFUL.toInt()) {
			writeLineToLogFile("TEST PASSED\n\n");
		}
		else {
			writeLineToLogFile("TEST FAILED\n\n");
		}
	}
	
	public void testGetEmptyKey(String key) {
		ReceivedResponseResult result = getGetResult(key);
		writeLineToLogFile("[ TEST GET with empty key. ]\n");
		if (result != null && result.resultCode == ReplyErrorCode.SUCCESSFUL.toInt()) {
//			writeLineToLogFile("Returned Value:" + result.returnValue + "\n");
			writeLineToLogFile("TEST PASSED\n\n");
		}
		else {
			writeLineToLogFile("TEST FAILED\n\n");
		}
	}
	
	public void testOnlyPut(String key, int numberOfBytes) {
		String dummyValue = "";
		
		writeLineToLogFile("[ TEST PUT when there is nothing. Key Length = " + key.length() + ". Value Length = " + numberOfBytes + " Bytes ]\n");
		for (int i = 0;i < numberOfBytes;i++) {
			dummyValue = dummyValue + "0"; 
		}
		if (getPutResultCode(key, dummyValue, 0) == ReplyErrorCode.SUCCESSFUL.toInt()) {
			writeLineToLogFile("TEST PASSED\n\n");
		}
		else {
			writeLineToLogFile("TEST FAILED\n\n");
		}
	}
	
	public void testOnlyGet(String key) {
		ReceivedResponseResult result = getGetResult(key);
		writeLineToLogFile("[ TEST GET when there is nothing. ]\n");
		if (result != null && result.resultCode == ReplyErrorCode.NONEXISTKEY.toInt()) {
			writeLineToLogFile("TEST PASSED\n\n");
		}
		else {
			writeLineToLogFile("TEST FAILED\n\n");
		}
	}
	
	public void testOnlyRemove(String key) {
		writeLineToLogFile("[ TEST REMOVE when there is nothing. ]\n");
		if (getRemoveResultCode(key) == ReplyErrorCode.NONEXISTKEY.toInt()) {
			writeLineToLogFile("TEST PASSED\n\n");
		}
		else {
			writeLineToLogFile("TEST FAILED\n\n");
		}
	}
	
	public void testOnlyShutdown() {
		writeLineToLogFile("[ TEST Shutdown. ]\n");

		getCommandResultCode(RequestCommand.SHUTDOWN.toInt());
	}
	
	public void testOnlyWipeout() {
		writeLineToLogFile("[ TEST Wipeout. ]\n");
		if (getCommandResultCode(RequestCommand.WIPEOUT.toInt()) == ReplyErrorCode.SUCCESSFUL.toInt()) {
			writeLineToLogFile("TEST PASSED\n\n");
		}
		else {
			writeLineToLogFile("TEST FAILED\n\n");
		}
	}
	
	public void testOnlyServerAlive() {
		writeLineToLogFile("[ TEST isAlive. ]\n");
		if (getCommandResultCode(RequestCommand.ISALIVE.toInt()) == ReplyErrorCode.SUCCESSFUL.toInt()) {
			writeLineToLogFile("TEST PASSED\n\n");
		}
		else {
			writeLineToLogFile("TEST FAILED\n\n");
		}
	}
	
	public void testOnlyInvaildCommand() {
		writeLineToLogFile("[ TEST Invalid Command. ]\n");
		if (getCommandResultCode(RequestCommand.INVALIDCOMMAND.toInt()) == ReplyErrorCode.UNRECOGN.toInt()) {
			writeLineToLogFile("TEST PASSED\n\n");
		}
		else {
			writeLineToLogFile("TEST FAILED\n\n");
		}
	}
	
	public void testPutGet(String key, int numberOfBytes, int expectedResult1, int expectedResult2) {
		int step1;
		int step2;
		int numberOfRetry = 0;
		String dummyValue = "";
		
		writeLineToLogFile("[ TEST Basic PUT/GET (PUT -> GET). Key Length = " + key.length() + ". Value Length = " + numberOfBytes + " Bytes ]\n");
		for (int i = 0;i < numberOfBytes;i++) {
			dummyValue = dummyValue + "0"; 
		}
		step1 = getPutResultCode(key, dummyValue, 0);
		writeLineToLogFile("Put Result: " + step1 + " v.s. Expected: " + expectedResult1 + "\n");
//		step2 = getGetResultCodeWithValueTest(key, dummyValue); // Check if the retrieved value is the value created in last step
		
		ReceivedResponseResult result = getGetResult(key);
		if (result != null) {
//			writeLineToLogFile("Result Code returned: " + result.resultCode + "\n");
			if (expectedResult2 == ReplyErrorCode.SUCCESSFUL.toInt() && !result.returnValue.equals(dummyValue)) {
				writeLineToLogFile("Get: Actual: " + result.returnValue + " v.s. Expected: " + dummyValue + "\n");
				step2 = WRONGRESULT;
			}
			else {
				step2 = result.resultCode;
			}
			numberOfRetry = result.numberOfRetry;
		}
		else {
			writeLineToLogFile("No Result returned\n");
			numberOfRetry = 3;
			step2 = EXCEPTION;
		}
		writeLineToLogFile("Get Result: " + step2 + " v.s. Expected: " + expectedResult2 + "\n");
		writeLineToLogFile("Number of Retry: " + numberOfRetry + "\n");
		
//		writeLineToLogFile("Get Result Code" + step2 + "\n");
		if (step1 == expectedResult1 && step2 == expectedResult2) {
			writeLineToLogFile("TEST PASSED\n\n");
		}
		else {
			writeLineToLogFile("TEST FAILED\n\n");
		}
	}
	
	public void testPutGetClosedLoop(int times) {
		int step1;
		int step2;
		int numberOfRetry = 0;
		int numberOfSuccess = 0;
		int numberOfFailure = 0;
		int numberOfTimeout = 0;
		String dummyKey = "";
		String dummyValue = "";
	    // creating a new object of the class Date  
	    java.util.Date date = new java.util.Date();    

		long throughputStartTime;
		long throughputEndTime;
		long startTime;
		long endTime;
	    long min = -1;
	    long max = 0;
	    double avg;
	    double sumThroughput = 0;
	    double sumGoodput = 0;
	    double standardDeviation;
	    double throughput = 0;
	    double goodput = 0;
	    long[] runningTime = new long[times]; 
	    long[] putRunningTime = new long[times]; 
	    long[] getRunningTime = new long[times]; 
	    long[] putGetRunningTime = new long[times * 2]; 

		writeLineToLogFile("[ TEST One Client Performance (1 clients, " + times + " closed loops per client): PUT -> GET (closed loop) ]\n");
		
		testOnlyServerAlive();
		testOnlyWipeout();
		writeLineToLogFile("Clients: 1\n");
//		Total Requests (without replies): 200
//		Total Requests (including replies): 200
		throughputStartTime = System.currentTimeMillis();
		for (int i = 0;i < times;i++) {
			dummyKey = generateRandomString(32);
			dummyValue = generateRandomString(32);

			startTime = System.currentTimeMillis();
			step1 = getPutResultCode(dummyKey, dummyValue, 0);
			endTime = System.currentTimeMillis();
			putRunningTime[i] = endTime - startTime;

//			writeLineToLogFile(i + ": Put " + dummyKey + " : " + dummyValue);

			startTime = System.currentTimeMillis();
//			step2 = getGetResultCodeWithValueTest(dummyKey, dummyValue); // Check if the retrieved value is the value created in last step

			ReceivedResponseResult result = getGetResult(dummyKey);
			if (result != null) {
				if (!result.returnValue.equals(dummyValue)) {
					writeLineToLogFile("Get: Actual: " + result.returnValue + " v.s. Expected: " + dummyValue + "\n");
					step2 = WRONGRESULT;
				}
				else {
					step2 = result.resultCode;
				}
				numberOfRetry += result.numberOfRetry;
			}
			else {
				numberOfRetry += 3;
				step2 = EXCEPTION;
			}
			writeLineToLogFile(i + ": Accumulated Number of Retry (Successful requests): " + numberOfRetry + "\n");

			endTime = System.currentTimeMillis();
			getRunningTime[i] = endTime - startTime;

			if (step1 == ReplyErrorCode.SUCCESSFUL.toInt() && step2 == ReplyErrorCode.SUCCESSFUL.toInt()) {
				numberOfSuccess++;
				numberOfSuccess++;
				sumGoodput += putRunningTime[i] + getRunningTime[i];
				putGetRunningTime[i] = putRunningTime[i];
				putGetRunningTime[i + 1] = getRunningTime[i];
				
				// Only count for successful cases
				if(min == -1) min = putRunningTime[i];

	            if (putRunningTime[i] > max) {
	                max = putRunningTime[i];
	            }
	            if (putRunningTime[i] < min) {
	                min = putRunningTime[i];
	            }
	            if (getRunningTime[i] > max) {
	                max = getRunningTime[i];
	            }
	            if (getRunningTime[i] < min) {
	                min = getRunningTime[i];
	            }
			}
			else if (step1 == TIMEOUT && step2 == TIMEOUT) {
				numberOfTimeout++;
				numberOfTimeout++;
			}
			else if (step1 == TIMEOUT || step2 == TIMEOUT) {
				writeLineToLogFile(i + ": Put Result Code" + step1 + "\n");
				writeLineToLogFile(i + ": Get Result Code" + step2 + "\n");
				numberOfTimeout++;
			}
			else if (step1 != ReplyErrorCode.SUCCESSFUL.toInt() && step2 != ReplyErrorCode.SUCCESSFUL.toInt()) {
				writeLineToLogFile(i + ": Put Result Code" + step1 + "\n");
				writeLineToLogFile(i + ": Get Result Code" + step2 + "\n");
				numberOfFailure++;
				numberOfFailure++;
			}
			else if (step1 == ReplyErrorCode.SUCCESSFUL.toInt() && step2 != ReplyErrorCode.SUCCESSFUL.toInt()) {
				writeLineToLogFile(i + ": Put Result Code" + step1 + "\n");
				writeLineToLogFile(i + ": Get Result Code" + step2 + "\n");
				sumGoodput += putRunningTime[i];
				numberOfFailure++;
			}
			else if (step1 != ReplyErrorCode.SUCCESSFUL.toInt() && step2 == ReplyErrorCode.SUCCESSFUL.toInt()) {
				writeLineToLogFile(i + ": Put Result Code" + step1 + "\n");
				writeLineToLogFile(i + ": Get Result Code" + step2 + "\n");
				sumGoodput += getRunningTime[i];
				numberOfFailure++;
			}

			endTime = System.currentTimeMillis();
			runningTime[i] = endTime - startTime;
		}
		throughputEndTime = System.currentTimeMillis();
		sumThroughput = throughputEndTime - throughputStartTime;

		standardDeviation = calculateStandardDeviation(times * 2, putGetRunningTime);
        avg = (float) sumGoodput/(times * 2);
        if (min == -1) min = 0;
        throughput = (double) (((times * 2) / sumThroughput) * 1000);
        if (sumGoodput != 0)
        	goodput = (double) (((times * 2) / sumGoodput) * 1000);

//		sum = endTimeOverall - startTimeOverall;
		writeLineToLogFile("Total Requests (without replies): " + (numberOfTimeout + numberOfFailure) + "\n");
		writeLineToLogFile("Total Requests (including replies): " + numberOfSuccess + "\n");
		writeLineToLogFile("Successful Responses: " + numberOfSuccess + "\n");
		writeLineToLogFile("Timeout Responses: " + numberOfTimeout + "\n");
		writeLineToLogFile("Total Failed Responses: " + numberOfFailure + "\n");
		writeLineToLogFile("% Success: " + (double) numberOfSuccess/(times*2)*100 + "\n");
		writeLineToLogFile("% Timeout: " + (double) numberOfTimeout/(times*2)*100 + "\n");
		writeLineToLogFile("% Failed: " + (double) numberOfFailure/(times*2)*100 + "\n");
		writeLineToLogFile("Response Time (Successful requests):\n");
		writeLineToLogFile("[max] " + max + "ms, [min] " + min + "ms\n");
		writeLineToLogFile("[avg] " + avg + "ms, [stdev] " + standardDeviation + "ms\n");
		writeLineToLogFile("Throughput: " + throughput + " requests per second\n");
		writeLineToLogFile("Goodput: " + goodput + " requests per second\n");
		writeLineToLogFile("Retry Rate (Successful requests): " + (double) numberOfRetry/numberOfSuccess + "\n");
	}
	
	public void testPutWipeoutGet(String key, int numberOfBytes) {
		int step1;
		int step2;
		int step3;
		String dummyValue = "";
		
		writeLineToLogFile("[ TEST Basic PUT/Wipeout/GET (PUT -> Wipeout -> GET). Key Length = " + key.length() + ". Value Length = " + numberOfBytes + " Bytes ]\n");
		for (int i = 0;i < numberOfBytes;i++) {
			dummyValue = dummyValue + "0"; 
		}
		step1 = getPutResultCode(key, dummyValue, 0);
		step2 = getCommandResultCode(RequestCommand.WIPEOUT.toInt());
		ReceivedResponseResult result = getGetResult(key);
		if (result != null) {
			step3 = result.resultCode;
		}
		else {
			writeLineToLogFile("No Result returned\n");
			step3 = EXCEPTION;
		}
		if (step1 == ReplyErrorCode.SUCCESSFUL.toInt() && step2 == ReplyErrorCode.SUCCESSFUL.toInt() && step3 == ReplyErrorCode.NONEXISTKEY.toInt()) {
			writeLineToLogFile("TEST PASSED\n\n");
		}
		else {
			writeLineToLogFile("TEST FAILED\n\n");
		}
	}
	
	public void testPutWipeoutRemove(String key, int numberOfBytes) {
		int step1;
		int step2;
		int step3;
		String dummyValue = "";
		
		writeLineToLogFile("[ TEST Basic PUT/Wipeout/REMOVE (PUT -> Wipeout -> REMOVE). Key Length = " + key.length() + ". Value Length = " + numberOfBytes + " Bytes ]\n");
		for (int i = 0;i < numberOfBytes;i++) {
			dummyValue = dummyValue + "0"; 
		}
		step1 = getPutResultCode(key, dummyValue, 0);
		step2 = getCommandResultCode(RequestCommand.WIPEOUT.toInt());
		step3 = getRemoveResultCode(key);
		if (step1 == ReplyErrorCode.SUCCESSFUL.toInt() && step2 == ReplyErrorCode.SUCCESSFUL.toInt() && step3 == ReplyErrorCode.NONEXISTKEY.toInt()) {
			writeLineToLogFile("TEST PASSED\n\n");
		}
		else {
			writeLineToLogFile("TEST FAILED\n\n");
		}
	}
	
	public void testPutGetRemoveGet(String key, int numberOfBytes) {
		int step1;
		int step2;
		int step3;
		int step4;
		String dummyValue = "";
		
		writeLineToLogFile("[ TEST Basic REMOVE (PUT -> GET -> REM -> GET). Key Length = " + key.length() + ". Value Length = " + numberOfBytes + " Bytes ]\n");
		for (int i = 0;i < numberOfBytes;i++) {
			dummyValue = dummyValue + "0"; 
		}
		
		step1 = getPutResultCode(key, dummyValue, 0);

		ReceivedResponseResult result = getGetResult(key);
		if (result != null) {
			if (!result.returnValue.equals(dummyValue)) {
				writeLineToLogFile("Get: Actual: " + result.returnValue + " v.s. Expected: " + dummyValue + "\n");
				step2 = WRONGRESULT;
			}
			else {
				step2 = result.resultCode;
			}
		}
		else {
			step2 = EXCEPTION;
		}
		
		step3 = getRemoveResultCode(key);
		
		result = getGetResult(key);
		step4 = result.resultCode;
		
		if (step1 == ReplyErrorCode.SUCCESSFUL.toInt() && step2 == ReplyErrorCode.SUCCESSFUL.toInt()
				&& step3 == ReplyErrorCode.SUCCESSFUL.toInt() && step4 == ReplyErrorCode.NONEXISTKEY.toInt()) {
			writeLineToLogFile("TEST PASSED\n\n");
		}
		else {
			writeLineToLogFile("TEST FAILED\n\n");
		}
	}
	
	public int getCommandResultCode(int command) {
		Builder builder = KeyValueRequest.KVRequest.newBuilder();
		
		builder.setCommand(command);
		Msg msg = messageHandle.getMessage(builder.build().toByteArray());
		
		try {
			ReceivedRequest receivedMsg = protocolHandle.sendAndReceive(InetAddress.getByName(serverIPAddress), serverPortNumber, msg,1000,5,4);
			
			if ( receivedMsg != null ) {
				KVResponse response = KVResponse.parseFrom(receivedMsg.message.getPayload());
//				System.out.println("Response Code : " + response.getErrCode());
				return response.getErrCode();
			}
			return NORESPONSE;
		} catch (java.net.SocketTimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			writeLineToLogFile("Exception : " + e.toString() + "\n");
			return TIMEOUT;
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			writeLineToLogFile("Exception : " + e.toString() + "\n");
			return EXCEPTION;
		} catch (InvalidProtocolBufferException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			writeLineToLogFile("Exception : " + e.toString() + "\n");
			return EXCEPTION;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			writeLineToLogFile("Exception : " + e.toString() + "\n");
			return EXCEPTION;
		}
	}
	
//	public int testInvalidCommand() {
//		Builder builder = KeyValueRequest.KVRequest.newBuilder();
//		
//		builder.setCommand(RequestCommand.INVALIDCOMMAND.toInt());
//		Msg msg = messageHandle.getMessage(builder.build().toByteArray());
//		
//		try {
//			ReceivedRequest receivedMsg = protocolHandle.sendAndReceive(InetAddress.getByName(serverIPAddress), serverPortNumber, msg,1000,5,4);
//			
//			if ( receivedMsg != null ) {
//				KVResponse response = KVResponse.parseFrom(receivedMsg.message.getPayload());
//				System.out.println("Response Code : " + response.getErrCode());
//				return response.getErrCode();
//			}
//			return NORESPONSE;
//		} catch (UnknownHostException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return EXCEPTION;
//		} catch (InvalidProtocolBufferException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return EXCEPTION;
//		}
//	}
	
	public int getRemoveResultCode(String key) {
		Builder builder = KeyValueRequest.KVRequest.newBuilder();
		
		builder.setCommand(RequestCommand.REMOVE.toInt());
		builder.setKey(ByteString.copyFromUtf8(key));
	
		Msg msg = messageHandle.getMessage(builder.build().toByteArray());
		
		try {
			ReceivedRequest receivedMsg = protocolHandle.sendAndReceive(InetAddress.getByName(serverIPAddress), serverPortNumber, msg,1000,5,4);
			
			if ( receivedMsg != null ) {
				KVResponse response = KVResponse.parseFrom(receivedMsg.message.getPayload());
				
//				System.out.println("Response Code : " + response.getErrCode());
				return response.getErrCode();
			}
			return NORESPONSE;
		} catch (java.net.SocketTimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			writeLineToLogFile("getRemoveResultCode Exception : " + e.toString() + "\n");
			return TIMEOUT;
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			writeLineToLogFile("getRemoveResultCode Exception : " + e.toString() + "\n");
			return EXCEPTION;
		} catch (InvalidProtocolBufferException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			writeLineToLogFile("getRemoveResultCode Exception : " + e.toString() + "\n");
			return EXCEPTION;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			writeLineToLogFile("getRemoveResultCode Exception : " + e.toString() + "\n");
			return EXCEPTION;
		}
	}

//	public int getGetResultCode(String key) {
//		Builder builder = KeyValueRequest.KVRequest.newBuilder();
//		
//		builder.setCommand(RequestCommand.GET.toInt());
//		builder.setKey(ByteString.copyFromUtf8(key));
//		
//		Msg msg = messageHandle.getMessage(builder.build().toByteArray());
//		
//		try {
//			ReceivedRequest receivedMsg = protocolHandle.sendAndReceive(InetAddress.getByName(serverIPAddress), serverPortNumber, msg,1000,5,4);
//			
//			if ( receivedMsg != null ) {
//				KVResponse response = KVResponse.parseFrom(receivedMsg.message.getPayload());
//				
////				System.out.println("Response Code : " + response.getErrCode());
//				if ( response.hasValue() && response.hasVersion()) {
//					System.out.println("Get Value : " + response.getValue().toStringUtf8() + " Version : " + response.getVersion());
//				}
//				else {
//					System.out.println("No Value");
//				}
//				return response.getErrCode();
//			}
//			return NORESPONSE;
//		} catch (java.net.SocketTimeoutException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			writeLineToLogFile("Exception : " + e.toString() + "\n");
//			return TIMEOUT;
//		} catch (UnknownHostException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			writeLineToLogFile("Exception : " + e.toString() + "\n");
//			return EXCEPTION;
//		} catch (InvalidProtocolBufferException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			writeLineToLogFile("Exception : " + e.toString() + "\n");
//			return EXCEPTION;
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			writeLineToLogFile("Exception : " + e.toString() + "\n");
//			return EXCEPTION;
//		}
//	}
//	
	public ReceivedResponseResult getGetResult(String key) {
		Builder builder = KeyValueRequest.KVRequest.newBuilder();
		
		builder.setCommand(RequestCommand.GET.toInt());
		builder.setKey(ByteString.copyFromUtf8(key));

	    Msg msg = messageHandle.getMessage(builder.build().toByteArray());
		
		ReceivedResponseResult result = null;
		
	    // creating a new object of the class Date  
	    java.util.Date date = new java.util.Date();    

	    try {
	        ReceivedRequest receivedMsg = protocolHandle.sendAndReceive(InetAddress.getByName(serverIPAddress), serverPortNumber, msg,1000,5,4);
			
			if ( receivedMsg != null ) {
				KVResponse response = KVResponse.parseFrom(receivedMsg.message.getPayload());
				
//				System.out.println("Response Code : " + response.getErrCode());
//				if (response.hasValue() && response.hasVersion()) {
//					System.out.println("Get Value : " + response.getValue().toStringUtf8() + " Version : " + response.getVersion());
//				}
//				else {
//					System.out.println("No Value");
//				}
//				if (response.hasValue()) {
////					writeLineToLogFile(date + " : Get: Actual: " + response.getValue().toStringUtf8() + " v.s. Expected: " + value + "\n");
//					if (!response.getValue().toStringUtf8().equals(value)) {
////						return WRONGRESULT;
//					}
//				}
				result = new ReceivedResponseResult(response.getErrCode(), response.getValue().toStringUtf8(), receivedMsg.numberOfRetry);
				return result;
//				return response.getErrCode();
			}
		} catch (java.net.SocketTimeoutException e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			writeLineToLogFile("getGetResultCodeWithValueTest Exception : " + e.toString() + "\n");
	        try {
				e.printStackTrace(new PrintStream(new FileOutputStream(logFilePath, true), true));
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			writeLineToLogFile("getGetResultCodeWithValueTest Exception : " + e.toString() + "\n");
	        try {
				e.printStackTrace(new PrintStream(new FileOutputStream(logFilePath, true), true));
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} catch (InvalidProtocolBufferException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			writeLineToLogFile("getGetResultCodeWithValueTest Exception : " + e.toString() + "\n");
	        try {
				e.printStackTrace(new PrintStream(new FileOutputStream(logFilePath, true), true));
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			writeLineToLogFile("getGetResultCodeWithValueTest Exception : " + e.toString() + "\n");
	        try {
				e.printStackTrace(new PrintStream(new FileOutputStream(logFilePath, true), true));
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	    return result;
	}
	
//	public boolean testGetResult(String key, String value, int expectedResult) {
//		ReceivedResponseResult result = getGetResult(key);
//		if (!result.returnValue.equals(value) ||) {
//			writeLineToLogFile("Get: Actual: " + result.returnValue + " v.s. Expected: " + value + "\n");
//			return false;
//		}
//		return true;
//	}
	
	public int getPutResultCode(String key, String value,Integer version) {
		Builder builder = KeyValueRequest.KVRequest.newBuilder();
		
		builder.setCommand(RequestCommand.PUT.toInt());
		builder.setKey(ByteString.copyFromUtf8(key));
		builder.setValue(ByteString.copyFromUtf8(value));
		if (version != null )
			builder.setVersion(version);
		
		Msg msg = messageHandle.getMessage(builder.build().toByteArray());
		
		try {
			ReceivedRequest receivedMsg = protocolHandle.sendAndReceive(InetAddress.getByName(serverIPAddress), serverPortNumber, msg,1000,5,4);
			
			if ( receivedMsg != null ) {
				KVResponse response = KVResponse.parseFrom(receivedMsg.message.getPayload());
				
//				System.out.println("Put Key: " + key + " Value: " + value + " Version : " + version);
//				System.out.println("Put Response Code : " + response.getErrCode());
				return response.getErrCode();
			}
			return NORESPONSE;
		} catch (java.net.SocketTimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			writeLineToLogFile("getPutResultCode Exception : " + e.toString() + "\n");
	        try {
				e.printStackTrace(new PrintStream(new FileOutputStream(logFilePath, true), true));
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return TIMEOUT;
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			writeLineToLogFile("getPutResultCode Exception : " + e.toString() + "\n");
	        try {
				e.printStackTrace(new PrintStream(new FileOutputStream(logFilePath, true), true));
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return EXCEPTION;
		} catch (InvalidProtocolBufferException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			writeLineToLogFile("getPutResultCode Exception : " + e.toString() + "\n");
	        try {
				e.printStackTrace(new PrintStream(new FileOutputStream(logFilePath, true), true));
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return EXCEPTION;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			writeLineToLogFile("getPutResultCode Exception : " + e.toString() + "\n");
	        try {
				e.printStackTrace(new PrintStream(new FileOutputStream(logFilePath, true), true));
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return EXCEPTION;
		}
	}
	
//	public int testIsAlive() {
//		Builder builder = KeyValueRequest.KVRequest.newBuilder();
//		
//		builder.setCommand(RequestCommand.ISALIVE.toInt());
//		Msg msg = messageHandle.getMessage(builder.build().toByteArray());
//		
//		try {
//			ReceivedRequest receivedMsg = protocolHandle.sendAndReceive(InetAddress.getByName(serverIPAddress), serverPortNumber, msg,1000,5,4);
//			
//			if ( receivedMsg != null ) {
//				KVResponse response = KVResponse.parseFrom(receivedMsg.message.getPayload());
//				
//				System.out.println("Response Code : " + response.getErrCode());
//				return response.getErrCode();
//			}
//			return NORESPONSE;
//		} catch (UnknownHostException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return EXCEPTION;
//		} catch (InvalidProtocolBufferException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return EXCEPTION;
//		}
//	}
	
	public void testOnlyGetProcessID() {
		Builder builder = KeyValueRequest.KVRequest.newBuilder();
		
		builder.setCommand(RequestCommand.GETPID.toInt());
		Msg msg = messageHandle.getMessage(builder.build().toByteArray());
		writeLineToLogFile("[ TEST GetProcessID. ]\n");
		
		try {
			ReceivedRequest receivedMsg = protocolHandle.sendAndReceive(InetAddress.getByName(serverIPAddress), serverPortNumber, msg,1000,5,4);
			
			if ( receivedMsg != null ) {
				KVResponse response = KVResponse.parseFrom(receivedMsg.message.getPayload());
				
//				System.out.println("Response Code : " + response.getErrCode());
				if (response.getErrCode() == ReplyErrorCode.SUCCESSFUL.toInt() && response.hasPid()) {
					System.out.println("Get Process ID: " + response.getPid());
					writeLineToLogFile("TEST PASSED\n\n");
				}
				else {
					writeLineToLogFile("TEST FAILED\n\n");
				}
				writeLineToLogFile("");
			}
		} catch (java.net.SocketTimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			writeLineToLogFile("Exception : " + e.toString() + "\n");
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			writeLineToLogFile("Exception : " + e.toString() + "\n");
		} catch (InvalidProtocolBufferException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			writeLineToLogFile("Exception : " + e.toString() + "\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			writeLineToLogFile("Exception : " + e.toString() + "\n");
		}
	}
	
	public void testOnlyGetMembershipCount() {
		Builder builder = KeyValueRequest.KVRequest.newBuilder();
		
		builder.setCommand(RequestCommand.GETMEMBERSHIPCOUNT.toInt());
		Msg msg = messageHandle.getMessage(builder.build().toByteArray());
		writeLineToLogFile("[ TEST GetMembershipCount. ]\n");
		
		try {
			ReceivedRequest receivedMsg = protocolHandle.sendAndReceive(InetAddress.getByName(serverIPAddress), serverPortNumber, msg,1000,5,4);
			
			if ( receivedMsg != null ) {
				KVResponse response = KVResponse.parseFrom(receivedMsg.message.getPayload());
				
//				System.out.println("Response Code : " + response.getErrCode());
				if (response.getErrCode() == ReplyErrorCode.SUCCESSFUL.toInt() && response.hasMembershipCount() && response.getMembershipCount() == 1) {
					System.out.println("Get Membership Count: " + response.getMembershipCount());
					writeLineToLogFile("TEST PASSED\n\n");
				}
				else {
					writeLineToLogFile("TEST FAILED\n\n");
				}
				writeLineToLogFile("");
			}
		} catch (java.net.SocketTimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			writeLineToLogFile("Exception : " + e.toString() + "\n");
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			writeLineToLogFile("Exception : " + e.toString() + "\n");
		} catch (InvalidProtocolBufferException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			writeLineToLogFile("Exception : " + e.toString() + "\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			writeLineToLogFile("Exception : " + e.toString() + "\n");
		}
	}
	
//	public int testWipeout() {
//		Builder builder = KeyValueRequest.KVRequest.newBuilder();
//		
//		builder.setCommand(RequestCommand.WIPEOUT.toInt());
//		Msg msg = messageHandle.getMessage(builder.build().toByteArray());
//		
//		try {
//			ReceivedRequest receivedMsg = protocolHandle.sendAndReceive(InetAddress.getByName(serverIPAddress), serverPortNumber, msg,1000,5,4);
//			
//			if ( receivedMsg != null ) {
//				KVResponse response = KVResponse.parseFrom(receivedMsg.message.getPayload());
//				
//				System.out.println("Response Code : " + response.getErrCode());
//				return response.getErrCode();
//			}
//			return NORESPONSE;
//		} catch (UnknownHostException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return EXCEPTION;
//		} catch (InvalidProtocolBufferException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return EXCEPTION;
//		}
//	}
	
//	public int testShutdown() {
//		MessageHandle messageHandle = new MessageHandle(clientPortNumber,16384);
//		RequestReplyProtocolHandle protocolHandle = new RequestReplyProtocolHandle(clientPortNumber,messageHandle  );
//		
//		Builder builder = KeyValueRequest.KVRequest.newBuilder();
//		
//		builder.setCommand(RequestCommand.SHUTDOWN.toInt());
//		Msg msg = messageHandle.getMessage(builder.build().toByteArray());
//		
//		try {
//			ReceivedRequest receivedMsg = protocolHandle.sendAndReceive(InetAddress.getByName(serverIPAddress), serverPortNumber, msg,1000,5,4);
//			
//			if ( receivedMsg != null ) {
//				KVResponse response = KVResponse.parseFrom(receivedMsg.message.getPayload());
//				
//				System.out.println("Response Code : " + response.getErrCode());
//				return response.getErrCode();
//			}
//			return NORESPONSE;
//		} catch (UnknownHostException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return EXCEPTION;
//		} catch (InvalidProtocolBufferException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return EXCEPTION;
//		}
//	}
	
	private boolean readServerListFile() {

		try {
			FileInputStream fstream = new FileInputStream(serverListFilePath);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;

			strLine = br.readLine(); // There is only one line in the file.
			String tokens[] = strLine.split(":"); 
			if (tokens.length == 2) {
				serverIPAddressFromFile = tokens[0];
				serverPortNumberFromFile = tokens[1];
//				System.out.println("Server Address from File: " + serverIPAddressFromFile);
//				System.out.println("Port Number from File: " + serverPortNumberFromFile);

				in.close();
				return true;
			}
			else {
//				System.out.println("Invalid Server List File.");
			}
			in.close();
			return false;
		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
			return false;
		}
	}

	private void writeLineToLogFile(String line) {

	    try {
	    	boolean append = true;

	    	if (firstWrite) {
	    		firstWrite = false;
	    		append = false;
	    	}
	    	BufferedWriter writer = new BufferedWriter(new FileWriter(logFilePath, append));
	    	// OR: BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filename, append)));

	    	writer.write(line);

	    	writer.close();
	    } catch (IOException e) {
			System.err.println("Error: " + e.getMessage());
	        e.printStackTrace();
	    }
	}

	public String generateRandomString(int length) {
		 
	    int leftLimit = 97; // letter 'a'
	    int rightLimit = 122; // letter 'z'
	    int targetStringLength = length;
	    Random random = new Random();
	    StringBuilder buffer = new StringBuilder(targetStringLength);
	    for (int i = 0; i < targetStringLength; i++) {
	        int randomLimitedInt = leftLimit + (int) 
	          (random.nextFloat() * (rightLimit - leftLimit + 1));
	        buffer.append((char) randomLimitedInt);
	    }
	    String generatedString = buffer.toString();
	    return generatedString;

//	    System.out.println(generatedString);
	}

	static double calculateStandardDeviation(int n,long in[])
	{
	    double sum = 0;  
		for(int i = 0;i < n;i++) 
		{
			sum = sum + in[i];
		}
		double mean = sum / n;
//	    System.out.println("Mean: "+ mean);
		sum = 0;
		for(int i = 0;i < n;i++) 
		{
			sum += Math.pow((in[i] - mean), 2);
		}
		mean = (float) sum / (n-1);
		double deviation = (double) Math.sqrt(mean);
//		System.out.println("standard deviation :" + deviation);
		return deviation;
	}
}
