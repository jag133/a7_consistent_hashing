package com.s85040533.CPEN431.A3.CPEN431_2022_A3;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class ServerNode {
    private InetAddress address;
    private int id = 0;
    private int port;
    private int hashValue;

    public ServerNode(String host, int port, int id, int hashValue) throws UnknownHostException {  
        this.address = InetAddress.getByName(host);
        this.port = port;
        this.id = id;
        this.hashValue = hashValue;
    }
    
    public ServerNode(String nodeListEntry, int id) throws IllegalArgumentException, UnknownHostException {
		String[] args = nodeListEntry.split(":");
        this.address = InetAddress.getByName(args[0]);
        this.port = Integer.parseInt(args[1]);
        this.id = id;
        this.hashValue = Integer.parseInt(args[2]);
	}

    
    public boolean equals(Object o) {
        if (o == this) return true;
        if (! (o instanceof ServerNode)) return false;

        ServerNode other = (ServerNode) o;        
        return other.getId() == this.id;
    }
    
    public int getId() {
    	return this.id;
    }

    public InetAddress getAddress() {
        return address;
    }

    public int getPort() {
        return port;
    }
    
    public int getHashValue() {
    	return hashValue;
    }

}
