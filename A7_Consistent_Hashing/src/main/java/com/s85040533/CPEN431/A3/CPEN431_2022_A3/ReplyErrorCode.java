package com.s85040533.CPEN431.A3.CPEN431_2022_A3;

public enum ReplyErrorCode {
	
	SUCCESSFUL(0),NONEXISTKEY(1),OUTOFSPACE(2),OVERLOAD(3),INTERNALFAILURE(4),UNRECOGN(5), INVALIDKEY(6), INVALIDVAL(7); 
	private final int code;

	private ReplyErrorCode(int code) {
        this.code = code;
    }

	public int toInt() {
	    return code;
	}
}
