package com.s85040533.CPEN431.A3.CPEN431_2022_A3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import com.s85040533.CPEN431.A3.CPEN431_2022_A3.ServerNode;

/**
 * Hello world!
 *
 */
public class App 
{
	public static int memoryLimit = 4 * 1024 * 1024;
	public static int cacheLimit = 4 * 1024 *1024;
	public static int minKVStoreSize = 47 * 1024 * 1024;
	public static int overloadWaitTime = 1000;
	public static int numberOfThread = 8;
	public static int numberOfTasks = 2000;
	public static int gcTime = 2000;
	public static String fileName;
	public static int portNumber;
	public static KeyValueHandle keyValueHandle;
    public static ConcurrentHashMap<Integer, ServerNode> hashCircle;

    public static void main( String[] args ) throws IOException
    {
		if (args.length == 2) {
//			memoryLimit = Integer.parseInt(args[0]) * 1024 * 1024;
//			cacheLimit = Integer.parseInt(args[1]) * 1024 * 1024;
//			minKVStoreSize = Integer.parseInt(args[2]) * 1024 * 1024;
//			overloadWaitTime = Integer.parseInt(args[3]);
//			numberOfThread = Integer.parseInt(args[4]);
//			numberOfTasks = Integer.parseInt(args[5]);
//			gcTime = Integer.parseInt(args[6]);
			fileName = args[0];
			portNumber = Integer.parseInt(args[1]);
			System.out.println("Memory Limit: " + memoryLimit);
			System.out.println("Cache Limit: " + cacheLimit);
			System.out.println("Min KV Store Size: " + minKVStoreSize);
			System.out.println("Overload Wait Time: " + overloadWaitTime);
			System.out.println("Number of Threads: " + numberOfThread);
			System.out.println("Number of Tasks: " + numberOfTasks);
			System.out.println("GC Time Interval: " + gcTime);
			System.out.println("File name containing the NodeList: " + fileName);
			System.out.println("Port number of current node: " + portNumber);


		}
		
        List<ServerNode> nodes = loadNodesFromFile(fileName);
        hashCircle = new ConcurrentHashMap<Integer, ServerNode>();
        for (ServerNode node : nodes) {
        		hashCircle.put(node.getHashValue(), node);
        		System.out.println("Address" +node.getAddress() + "Port"+node.getPort());
        }

    	keyValueHandle = new KeyValueHandle(memoryLimit);
    	
    	try {
    		System.out.println("Creating ServerThread");
    		 new ServerThread(portNumber, keyValueHandle, hashCircle).run();
    		    
    	}catch (Exception exp) {
    		 System.out.println("error in main" + exp.getMessage());
    	}
       

    }
    
	private static List<ServerNode> loadNodesFromFile(String filename) throws IOException {
		List<ServerNode> nodes = 
				Collections.synchronizedList(new ArrayList<ServerNode>());

		FileReader fileReader = new FileReader(filename);
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		String line;
		int id = 0;

		while ((line = bufferedReader.readLine()) != null) {

			try {
				if (!line.trim().isEmpty()) {
					nodes.add(new ServerNode(line,id));
					id++;
				}
            } catch (Exception e) {
			    e.printStackTrace();
            }
		}

        bufferedReader.close();

		return nodes;
	}
	
    
}
