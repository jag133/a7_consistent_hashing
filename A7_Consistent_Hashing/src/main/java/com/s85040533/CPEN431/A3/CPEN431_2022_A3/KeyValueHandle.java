package com.s85040533.CPEN431.A3.CPEN431_2022_A3;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;

import com.google.protobuf.ByteString;

import ca.NetSysLab.ProtocolBuffers.KeyValueRequest.KVRequest;
import ca.NetSysLab.ProtocolBuffers.KeyValueResponse.KVResponse.Builder;

public class KeyValueHandle {

	
	public int memoryLimit;
	public int numberOfByteUsed;
	ConcurrentHashMap<ByteString, CacheObject> cache;
	// creating a new object of the class Date  
	private java.util.Date date = new java.util.Date();    
 
	public KeyValueHandle(int memLimit) {
		
	 
		//TODO : Load factor will affect the performance, Let turn it when there is a performance hit
		cache = new ConcurrentHashMap<ByteString, CacheObject>();
		memoryLimit = memLimit;
		numberOfByteUsed =0;
		 
	}
	
	public boolean process(KVRequest kvRequest,  Builder responseBuilder) {
 
		boolean isSuccess = false;
		if ( kvRequest != null ) {
			
			if ( kvRequest.hasCommand() ) {
				
				int command = kvRequest.getCommand();
				//System.out.println(" Command : " + command + " Memory : " + Runtime.getRuntime().freeMemory());
				
				if (  command ==  RequestCommand.PUT.toInt()) {
					isSuccess= putOperation(kvRequest, responseBuilder);
				}else if ( command == RequestCommand.GET.toInt()) {
					isSuccess= getOperation(kvRequest, responseBuilder);
					
				}else if ( command == RequestCommand.REMOVE.toInt()) {
					isSuccess= removeOperation(kvRequest, responseBuilder);
					
				}else if (  command == RequestCommand.WIPEOUT.toInt()) {
					cache.clear();
					System.gc();
					numberOfByteUsed =0;
					isSuccess = true;
				}else {
					responseBuilder.setErrCode(ReplyErrorCode.UNRECOGN.toInt());
				}
				
				
			}else {
				responseBuilder.setErrCode(ReplyErrorCode.UNRECOGN.toInt());
			}
				
			
		
			
			
			
		}
		if ( isSuccess ) {
			responseBuilder.setErrCode(ReplyErrorCode.SUCCESSFUL.toInt());
		}else if (!responseBuilder.hasErrCode()    ) {
			responseBuilder.setErrCode(ReplyErrorCode.INTERNALFAILURE.toInt());
		
		}
		
		return isSuccess;
	}
	public boolean removeOperation(KVRequest kvRequest,  Builder responseBuilder) {
		boolean isSuccess = false;
		if (isGetOrRemoveRequestValid(kvRequest, responseBuilder) ) {
			
			CacheObject cacheObj = cache.remove(kvRequest.getKey());
			if ( cacheObj != null ) {
				isSuccess = true;
				numberOfByteUsed -= cacheObj.value.size();
			}else {
				responseBuilder.setErrCode(ReplyErrorCode.NONEXISTKEY.toInt());
			}
				
			
			
			
		}

		return isSuccess;
		
	}
	public boolean getOperation(KVRequest kvRequest,  Builder responseBuilder) {
		boolean isSuccess = false;
		if (isGetOrRemoveRequestValid(kvRequest, responseBuilder) ) {
			
			CacheObject cacheObj = cache.get(kvRequest.getKey());
			if ( cacheObj != null ) {
				
			 
					 
						
//				responseBuilder.setValue(ByteString.copyFrom( CompressionUtils.decompress(cacheObj.value) ));
				responseBuilder.setValue(cacheObj.value);
				responseBuilder.setVersion(cacheObj.version);
				isSuccess = true;
			 
	  
		       
			}else {
				responseBuilder.setErrCode(ReplyErrorCode.NONEXISTKEY.toInt());
			}
				
			
			
			
		}

		return isSuccess;
		
	}
	
	public boolean putOperation(KVRequest kvRequest,  Builder responseBuilder) {
		boolean isSuccess = false;
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
		dateFormat.setTimeZone(TimeZone.getTimeZone("PST"));
		String currentTime;

		if (isPutRequestValid(kvRequest, responseBuilder) ) {
			Runtime runtime =  Runtime.getRuntime();
			
			if (runtime.maxMemory() - runtime.totalMemory() + runtime.freeMemory() >= memoryLimit && this.numberOfByteUsed < App.minKVStoreSize) {
				try {
					if ( putToCache(kvRequest )){
						isSuccess = true;
					}
				} catch (OutOfMemoryError oome) {
					responseBuilder.setErrCode(ReplyErrorCode.OVERLOAD.toInt());
	    			responseBuilder.setOverloadWaitTime(App.overloadWaitTime);
//	    	        System.out.println("Overload due to Out of Memory when PUT.");
//	    	        // creating a new object of the class Date  
//	    	        java.util.Date date = new java.util.Date();    
//	    			currentTime = dateFormat.format(date);
//	    			System.out.println(currentTime + " KV Store Used/Total: " + this.numberOfByteUsed + "/" + App.minKVStoreSize);
//	    			System.out.println(currentTime + " Cache Used/Total: " + KeyValueMessageResponseTask.cachedValueSize + "/" + App.cacheLimit);
//	    			System.out.println(currentTime + " Free/Used Memory: " + (runtime.maxMemory() - runtime.totalMemory() + runtime.freeMemory()) + "/" + (runtime.totalMemory() - runtime.freeMemory()));
		        }
			} else if ( this.numberOfByteUsed < App.minKVStoreSize ) {
				responseBuilder.setErrCode(ReplyErrorCode.OVERLOAD.toInt());
    			responseBuilder.setOverloadWaitTime(App.overloadWaitTime);
//    	        System.out.println("Overload! Free Memory below limit but KV Store is still not met.");
//    	        // creating a new object of the class Date  
//    	        java.util.Date date = new java.util.Date();    
//    			currentTime = dateFormat.format(date);
//    			System.out.println(currentTime + " KV Store Used/Total: " + this.numberOfByteUsed + "/" + App.minKVStoreSize);
//    			System.out.println(currentTime + " Cache Used/Total: " + KeyValueMessageResponseTask.cachedValueSize + "/" + App.cacheLimit);
//    			System.out.println(currentTime + " Free/Used Memory: " + (runtime.maxMemory() - runtime.totalMemory() + runtime.freeMemory()) + "/" + (runtime.totalMemory() - runtime.freeMemory()));
			}
			else {
				responseBuilder.setErrCode(ReplyErrorCode.OUTOFSPACE.toInt());
		        // creating a new object of the class Date  
//		        java.util.Date date = new java.util.Date();    
//				currentTime = dateFormat.format(date);
//				System.out.println(currentTime + " KV Store Used/Total: " + this.numberOfByteUsed + "/" + App.minKVStoreSize);
//				System.out.println(currentTime + " Cache Used/Total: " + KeyValueMessageResponseTask.cachedValueSize + "/" + App.cacheLimit);
//				System.out.println(currentTime + " Free/Used Memory: " + (runtime.maxMemory() - runtime.totalMemory() + runtime.freeMemory()) + "/" + (runtime.totalMemory() - runtime.freeMemory()));
			}
		}

		return isSuccess;
		
	}
	
	
	public boolean putToCache(KVRequest kvRequest){
		boolean result = false;
		if ( kvRequest != null ) {
			
			int version =0;
			if ( kvRequest.hasVersion())
				version = kvRequest.getVersion();
		 
			
			 
			//TODO : Always get the latest version and ignore old version (Retry failure)?
			CacheObject cacheObj = cache.get(kvRequest.getKey());
			if ( cacheObj != null  ) {
				numberOfByteUsed -= cacheObj.value.size();
				
			}
//			byte[] output = CompressionUtils.compress(kvRequest.getValue().toByteArray());
//			numberOfByteUsed += output.length;
//			cache.put(kvRequest.getKey(), new CacheObject(version,output));
			ByteString output = kvRequest.getValue();
			numberOfByteUsed += output.size();
			cache.put(kvRequest.getKey(), new CacheObject(version, output));
			result = true;
	       
			
		}
		
		return result;
		
	}
	protected boolean isGetOrRemoveRequestValid(KVRequest kvRequest, Builder responseBuilder) {
		boolean result = true;
		
		ReplyErrorCode error = null;
		if ( kvRequest != null ) {
			if (! kvRequest.hasKey() || kvRequest.getKey().size() > 32  )
			{
				 error = ReplyErrorCode.INVALIDKEY;
				
			} 
		}
		
		

		if ( error != null ) {
			responseBuilder.setErrCode(error.toInt());
			result = false;
		}
		
		return result;
	}
	protected boolean isPutRequestValid(KVRequest kvRequest, Builder responseBuilder) {
		boolean result = true;
		
		ReplyErrorCode error = null;
		if ( kvRequest != null ) {
			if (! kvRequest.hasKey() || kvRequest.getKey().size() > 32  )
			{
				 error = ReplyErrorCode.INVALIDKEY;
				
			}else if  ( !kvRequest.hasValue() || kvRequest.getValue().size() > 10000 ){
				error = ReplyErrorCode.INVALIDVAL;
			}
		}
		
		if ( error != null ) {
			responseBuilder.setErrCode(error.toInt());
			result = false;
		}
		
		
		return result;
	}
}
