package com.s85040533.CPEN431.A3.CPEN431_2022_A3;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.s85040533.CPEN431.A3.CPEN431_2022_A3.RequestReplyProtocolHandle.ReceivedRequest;

import ca.NetSysLab.ProtocolBuffers.KeyValueRequest.KVRequest;
import ca.NetSysLab.ProtocolBuffers.KeyValueResponse.KVResponse;
import ca.NetSysLab.ProtocolBuffers.KeyValueResponse.KVResponse.Builder;
import ca.NetSysLab.ProtocolBuffers.Message;
import ca.NetSysLab.ProtocolBuffers.Message.Msg;

import com.google.protobuf.ByteString;

public class ServerThread extends Thread {
  
 
   private short portNum;
   protected boolean isRunning;
   protected KeyValueHandle valueHandle ;
   protected RequestReplyProtocolHandle protocolHandle;
   protected MessageHandle msgHandle;
   protected ConcurrentHashMap<Integer, ServerNode> hashCircle;

	public ServerThread(int portNumber,KeyValueHandle keyValueHandle, ConcurrentHashMap<Integer, ServerNode> hashCircleMap) throws IOException {
		 this("ServerThread",portNumber,keyValueHandle, hashCircleMap);
	}
		 
    public ServerThread(String name,int portNumber,KeyValueHandle keyValueHandle, ConcurrentHashMap<Integer, ServerNode> hashCircleMap) throws IOException {
        super(name);
        portNum = (short) portNumber;
        msgHandle = new MessageHandle(portNum,14000);
        protocolHandle = new RequestReplyProtocolHandle(portNum,msgHandle);
        valueHandle = keyValueHandle;
        hashCircle = hashCircleMap;
        isRunning = true;

		Thread t = new Thread(new CacheCleaner());
		t.start();
    }
    
    public static int getPID() {
    	int result =0;
        String processName = java.lang.management.ManagementFactory.getRuntimeMXBean().getName();
        if (processName != null && processName.length() > 0) {
            
             result =  Integer.parseInt(processName.split("@")[0]);
        }
        return result;
    }	 
    public void run() {
        Builder responseBuilder = KVResponse.newBuilder();
        Msg repliedMessage = null;
        ReceivedRequest request = null;
        
        BlockingQueue<Runnable> queue = new ArrayBlockingQueue<Runnable>(App.numberOfTasks);
        ThreadPoolExecutor pool = new ThreadPoolExecutor(1, App.numberOfThread, 500L, TimeUnit.MILLISECONDS, queue);    	

        
        while (isRunning) {
            try {
                request = protocolHandle.receive(0);
                KeyValueMessageResponseTask task= new KeyValueMessageResponseTask( valueHandle,protocolHandle,msgHandle,request );
                                
        		KVRequest kvRequest = KVRequest.parseFrom(request.message.getPayload().toByteArray());
        		
        		if ((kvRequest.getCommand() == RequestCommand.GET.toInt()) ||
        				(kvRequest.getCommand() == RequestCommand.PUT.toInt())  || 
        				(kvRequest.getCommand() == RequestCommand.REMOVE.toInt()) ) {
        			
        			ServerNode correctNode = getCorrectNode(kvRequest, hashCircle);	
        			InetAddress localAddress = InetAddress.getLocalHost();
            		if (correctNode.getPort() != portNum || !correctNode.getAddress().equals(localAddress)) {
            			System.out.println("Rerouting to node "+correctNode.getPort());
            			reroute(request.message, request.requestIPAddress, request.requestPortNumber, correctNode);
            		} 
            		else {
            			System.out.println("Routing to selfnode "+correctNode.getPort());
                        pool.execute(task);
            		}
        			
        		} else {
        			System.out.println("Sever level command. Executing in current node");
        			pool.execute(task);
        		}
        		
        		
        		
        		
            } catch (java.util.concurrent.RejectedExecutionException e) {
    			responseBuilder.setErrCode(ReplyErrorCode.OVERLOAD.toInt());
    			responseBuilder.setOverloadWaitTime(App.overloadWaitTime);
             	repliedMessage = msgHandle.getMessage(responseBuilder.build().toByteArray(),request.message.getMessageID().toByteArray());
        		protocolHandle.send(request.requestIPAddress, request.requestPortNumber, repliedMessage);
            } catch (IOException e) {
                e.printStackTrace();
            	
            }
            // wait for termination        
        }
        pool.shutdown();

        System.exit(0);
       
    }
    
    
    
    //Hashing
    public static int getHash(ByteString key) {
    	return Math.floorMod(key.hashCode(), 256);
    }
    
    private static ServerNode getCorrectNode(KVRequest request, ConcurrentHashMap<Integer, ServerNode> hashCircle) {
    	System.out.println("Entering getCorrectNode method");
    	
        int hash = getHash(request.getKey());
    	System.out.println("Hash value of the key passed "+hash);
    	
        int original = hash;
		hash = (hash+1)%256;
    	while(hash != original) {
    		ServerNode node = hashCircle.get(hash);
    		if(node != null) {
    	    	System.out.println("Rerouted node details"+node.getAddress()+":"+node.getPort());
    			return node;
    		}
    		hash = (hash+1)%256;
    	}
    	return null;
    }
    
    private void reroute(Msg request, InetAddress clientAddr, int clientPort, ServerNode correctNode) throws IOException{
    	
    	System.out.println("Entering reroute method");
    	ByteString addr = ByteString.copyFrom(clientAddr.getAddress());

    	//Add client info using new protobuf
    	
    	Message.ClientInfo client = Message.ClientInfo.newBuilder()
    			.setAddress(addr)
    			.setPort(clientPort)
    			.build();

    	Msg newRequest = Msg.newBuilder()
    			.setMessageID(request.getMessageID())
    			.setPayload(request.getPayload())
    			.setCheckSum(request.getCheckSum())
    			.setClient(client)
    			.build();
    	
		Send(newRequest, correctNode.getAddress(), correctNode.getPort());
    }
    
    public static void Send(Msg msg, InetAddress address, int port) throws IOException {
       DatagramSocket socket = new DatagramSocket();

        // Serialize message
        DatagramPacket send_packet = new DatagramPacket(msg.toByteArray(), msg.getSerializedSize(), address, port);

        // Send packet
        socket.send(send_packet);
        socket.close();
    }
		 
    
}
