package com.s85040533.CPEN431.A3.CPEN431_2022_A3;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

public class CompressionUtils {
	 private static final int BUFFER_SIZE = 10000;

	    public static byte[] compress(final byte[] data)  
	    {
	        final Deflater deflater = new Deflater(Deflater.BEST_COMPRESSION, true);
//	        final Deflater deflater = new Deflater();
	        deflater.setLevel(Deflater.BEST_COMPRESSION);
	        deflater.setInput(data);

	        try (final ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length))
	        {
	            deflater.finish();
	            final byte[] buffer = new byte[BUFFER_SIZE];
	            while (!deflater.finished())
	            {
	                final int count = deflater.deflate(buffer);
	                outputStream.write(buffer, 0, count);
	            }

	            return outputStream.toByteArray();
	        } catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        return null;
	    }

	    public static byte[] decompress(final byte[] data)  
	    {
	        final Inflater inflater = new Inflater(true);
	        inflater.setInput(data);

	        try (final ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length))
	        {
	            byte[] buffer = new byte[BUFFER_SIZE];
	            while (!inflater.finished())
	            {
	                final int count = inflater.inflate(buffer);
	                outputStream.write(buffer, 0, count);
	            }

	            return outputStream.toByteArray();
	        } catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (DataFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        return null;
	    }
}
