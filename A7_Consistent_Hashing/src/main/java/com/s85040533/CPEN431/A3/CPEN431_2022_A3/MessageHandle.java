package com.s85040533.CPEN431.A3.CPEN431_2022_A3;

import java.io.IOException;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.Random;
import java.util.zip.CRC32;

import com.google.protobuf.ByteString;

import ca.NetSysLab.ProtocolBuffers.Message.Msg;

public class MessageHandle {
	private short clientPortNumber;
	private int maxPayload;
	private int messageIdSize;

	public MessageHandle(short portNumber, int maximumPayload) {
		clientPortNumber = portNumber;
		maxPayload = maximumPayload;
		messageIdSize= 16;
	}
	
	public int getMaxMessageSize()
	{
		return maxPayload + messageIdSize;
	}

	private byte[] getMessageId() {
		ByteBuffer result = ByteBuffer.allocate(messageIdSize);

		try {

			byte[] ipAddress = InetAddress.getLocalHost().getAddress();

			result.put(ipAddress);

			result.putShort(clientPortNumber);

			short randomNum = (short) new Random().nextInt(Short.MAX_VALUE + 1);
			result.putShort(randomNum);

			result.putLong(System.nanoTime());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result.array();
	}

	
	private long getCheckSum(byte[] messageId, byte[] payloadbytes)
	{
		byte[] messageContent = new byte[messageId.length + payloadbytes.length];
		 
		System.arraycopy(messageId, 0, messageContent, 0, messageId.length);
		System.arraycopy(payloadbytes, 0, messageContent, messageId.length, payloadbytes.length);
		
		CRC32 crc32 = new CRC32();
		crc32.update(messageContent);
		return crc32.getValue();
	}
	
	public boolean isValid(Msg message){
		return getCheckSum(message.getMessageID().toByteArray(), message.getPayload().toByteArray()) == message.getCheckSum();
	}
	
	public Msg getMessage(byte[] payloadbytes) {
		byte[] messageId = getMessageId();
 		Msg.Builder msg = Msg.newBuilder().setMessageID(ByteString.copyFrom(messageId)).setPayload(ByteString.copyFrom(payloadbytes)).setCheckSum(	getCheckSum(messageId, payloadbytes));
 		return msg.build();
	}
	public Msg getMessage(byte[] payloadbytes,byte[] messageId ) {
	
 		Msg.Builder msg = Msg.newBuilder().setMessageID(ByteString.copyFrom(messageId)).setPayload(ByteString.copyFrom(payloadbytes)).setCheckSum(	getCheckSum(messageId, payloadbytes));
 		return msg.build();
	}
}