package com.s85040533.CPEN431.A3.CPEN431_2022_A3;

public enum RequestCommand {
	
	PUT(1),GET(2),REMOVE(3),SHUTDOWN(4),WIPEOUT(5),ISALIVE(6),GETPID(7),GETMEMBERSHIPCOUNT(8),INVALIDCOMMAND(100);
	private final int code;

	private RequestCommand(int code) {
	        this.code = code;
	    }

    public int toInt() {
        return code;
    }
	
}
