Group Member Student IDs:

85919413

85040533

82478272

Points to note:			
1. We use 85919413 as the submission student ID for the evaluation client		
2. The A4.jar is under the base directory	
3. The A4EC2.log file is under the base directory also.		

Usage: 

Under base directory, run "java -Xmx64m -Xms64m -jar A4.jar 3 3 54 1500 2 2000 2200".

Here is the description for the args: 	
(minimum free memory mb) (cache size mb) (KV store limit mb) (server overload wait time ms) (number of threads) (number of server tasks limit) (GC interval)	

Brief description of our design:

To implement the at-most-once semantics in the server side, we design a server caching mechanism. We make use of a concurrent hashmap to cache the request message ids and the returning payload. Request of retrial with same message id will get the same payload as reply. We set a fixed size memory for this cache so as to leave the remaining memory for the KV store. We also set up a memory threshold to limit the maximum memory used for avoiding out of memory error. When the cache limit is reached, the server will signal the application to retry with overload error code and overload wait time. We have a mechanism to clear the cache regularly. When the loading drops, the cache will be cleared and later retries will succeed. We also extend command codes for testing purpose. To meet A4 additional requirements, we improve the program to support configurable target KV store size, number of threads, max. amount of tasks accepted in the queue of the threads and frequency of the cache cleaner. 

Below are some test cases we designed:

- We put all above mentioned configurable settings as arguments of the server program so that we can easy adjust them for testing / tuning.

- We add detail logs to show the changes of memory usage in KV store, cache, free memory left, used memory, etc, for identifying the root cause of memory problems.

---
